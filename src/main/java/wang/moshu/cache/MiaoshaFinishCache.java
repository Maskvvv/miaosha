package wang.moshu.cache;

import java.text.MessageFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import wang.moshu.constant.CommonConstant;
import wang.moshu.util.RedisUtil;

/**
 * 秒杀结束缓存
 *
 * @author xiangyong.ding@weimob.com
 * @category 秒杀结束缓存
 * @since 2017年4月18日 下午10:23:23
 */
@Component
public class MiaoshaFinishCache {
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 设定是否秒杀结束
     *
     * @param goodsId
     * @category @author xiangyong.ding@weimob.com
     * @since 2017年4月18日 下午10:26:27
     */
    public void setFinish(String goodsRandomName) {
        redisUtil.set(getKey(goodsRandomName), "");
    }

    /**
     * 指定商品秒杀是否结束
     *
     * @param goodsId
     * @return
     * @category 指定商品秒杀是否结束
     * @author xiangyong.ding@weimob.com
     * @since 2017年4月18日 下午10:31:01
     */
    public boolean isFinish(String goodsRandomName) {
        return redisUtil.get(getKey(goodsRandomName), String.class) != null;
    }

    private String getKey(String goodsRandomName) {
        // 生成物品 redis 键值
        String key = MessageFormat.format(CommonConstant.RedisKey.MIAOSHA_FINISH_FLAG, new Object[]{goodsRandomName});
        return key;
    }
}
